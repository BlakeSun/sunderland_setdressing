// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Sunderland_SetDresinTarget : TargetRules
{
	public Sunderland_SetDresinTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Sunderland_SetDresin" } );
	}
}
